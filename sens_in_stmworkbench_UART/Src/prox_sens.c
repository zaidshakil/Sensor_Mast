/******************************************************************************
 * \file prox_sens.c
 * \authors 
 * 
 * \brief 
 *
 ******************************************************************************/
 #include "prox_sens.h"
#include "debug.h"

 /******************************************************************************
 *                        	 LOCAL DEFINES
 ******************************************************************************/

 /******************************************************************************
 *                           LOCAL TYPES
 ******************************************************************************/

 /******************************************************************************
 *                           LOCAL DATAS
 ******************************************************************************/

 /******************************************************************************
 *                        LOCAL FUNCTIONS PROTOTYPES
 ******************************************************************************/

 /******************************************************************************
 *                        LOCAL FUNCTIONS 
 ******************************************************************************/

 /******************************************************************************
 *                        GLOBAL FUNCTIONS 
 ******************************************************************************/
///if 0 -> distance > 15 cm, if != 0 -> < 15cm
 uint8_t GP2Y0D815Z0F_get_value()
{
    return (uint8_t)HAL_GPIO_ReadPin(GPIOB, PS_SH_OUT_Pin);
}
//http://www.hobbytronics.co.uk/GP2Y0A60-distance-sensor
uint16_t GP2Y0A60SZLF_get_value_cm(uint16_t raw_value)
{
    uint8_t distance_cm = 0x00;
    
    if (raw_value > 2900)
        return 20;
    
    if (raw_value < 600)
        return 150;
    
    //convert voltage to cm
    if (raw_value > 1000)
    {
        distance_cm = (raw_value * (-0.015) + 65);
    }
    else
    {
        distance_cm = (raw_value * (-0.25) + 300);
    }
    
    return distance_cm;
}
void GP2Y0A60SZLF_enable(void)
{
	DEBUG_PRINT("[LOG] GP2Y0A60SZLF_enable");
    HAL_GPIO_WritePin(GPIOB, PS_EN_Pin, GPIO_PIN_SET);
}

void GP2Y0A60SZLF_disable(void)
{
  HAL_GPIO_WritePin(GPIOB, PS_EN_Pin, GPIO_PIN_RESET);  
  DEBUG_PRINT("[LOG] GP2Y0A60SZLF_disable");
}
 /******************************************************************************
 *                        END OF FILE
 ******************************************************************************/
