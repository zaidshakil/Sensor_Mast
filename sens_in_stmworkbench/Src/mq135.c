#include "stm32f4xx_hal.h"
#include <math.h>


#define RES_VALUE                   20000 // must be set on board
#define VOLTAGE_TOP_MV              5000
#define MQ135_SCALINGFACTOR         (double) 116.6020682 //CO2 gas value
#define MQ135_EXPONENT              (double) -2.769034857 //CO2 gas value

double get_co2_level(uint16_t voltage_lvl)
{
    double R_DIV_VAL = (VOLTAGE_TOP_MV - voltage_lvl) / voltage_lvl;
    
    return (double)MQ135_SCALINGFACTOR * pow((R_DIV_VAL), MQ135_EXPONENT);    
}