/******************************************************************************
 * \file HMC5883.c
 * \authors 
 * 
 * \brief 
 *
 ******************************************************************************/
#include "HMC5883.h"
#include "debug.h"
 /******************************************************************************
 *                        	 LOCAL DEFINES
 ******************************************************************************/
 ///device address
#define HMC5883_I2C_ADDR_W		(0x3C)
#define HMC5883_I2C_ADDR      	(0x3D)
#define HMC5883_I2C_TIMEOUT_MS  (500)
 
 ///register address
#define HMC5883_CFG_A_ADDR    (0x00)
#define HMC5883_CFG_B_ADDR    (0x01)
#define HMC5883_MODE_ADDR     (0x02)
#define HMC5883_X_MSB_ADDR    (0x03)
#define HMC5883_X_LSB_ADDR    (0x04)
#define HMC5883_Y_MSB_ADDR    (0x05)
#define HMC5883_Y_LSB_ADDR    (0x06)
#define HMC5883_Z_MSB_ADDR    (0x07)
#define HMC5883_Z_LSB_ADDR    (0x08)
#define HMC5883_STATUS_ADDR   (0x08)
 
 ///8-average, 15 Hz default or any other rate, normal measurement
#define HMC5883_DEF_CONF_VAL          (0x70)
///gain = 5
#define HMC5883_DEF_GAIN_VAL          (0xA0)
///single mode
#define HMC5883_DEF_SINGLE_MODE_VAL   (0x01)

  /******************************************************************************
 *                           LOCAL TYPES
 ******************************************************************************/

 /******************************************************************************
 *                           LOCAL DATAS
 ******************************************************************************/
 extern I2C_HandleTypeDef hi2c1;
  /******************************************************************************
 *                        LOCAL FUNCTIONS PROTOTYPES
 ******************************************************************************/

 /******************************************************************************
 *                        LOCAL FUNCTIONS 
 ******************************************************************************/

 /******************************************************************************
 *                        GLOBAL FUNCTIONS 
 ******************************************************************************/
 uint8_t HMC5883_init(HMC5883_mode_typedef mode)
 {
     uint8_t cmd[2] = {0};
     HAL_StatusTypeDef errocode = HAL_ERROR;
     
     errocode = HAL_I2C_IsDeviceReady(&hi2c1, HMC5883_I2C_ADDR, 5, HMC5883_I2C_TIMEOUT_MS);
     if (HAL_OK != errocode)
     {
    	 DEBUG_PRINT("[ERR] HMC5583: error HAL_I2C_IsDeviceReady errocode=%d", errocode);
    	 return errocode;
     }

     if (HMC5883_SINGLE_MODE == mode)
     {
    	 cmd[0] = HMC5883_CFG_A_ADDR;
         cmd[1] = HMC5883_DEF_CONF_VAL;

    	 errocode = HAL_I2C_Master_Transmit(&hi2c1, HMC5883_I2C_ADDR_W, cmd, 2, HMC5883_I2C_TIMEOUT_MS);
      	if (HAL_OK != errocode)
        {	
            DEBUG_PRINT("[ERR] HMC5583: error write to HMC5883_CFG_A_ADDR errocode=%d", errocode);
            return errocode;
        }

      	cmd[0] = HMC5883_CFG_B_ADDR;
        cmd[1] = HMC5883_DEF_GAIN_VAL;

        errocode = HAL_I2C_Master_Transmit(&hi2c1, HMC5883_I2C_ADDR_W, cmd, 2, HMC5883_I2C_TIMEOUT_MS);
        if (HAL_OK != errocode)
        {	
            DEBUG_PRINT("[ERR] HMC5583: error write to HMC5883_CFG_B_ADDR errocode=%d", errocode);
            return errocode;
        }
     }
     else
     {
         DEBUG_PRINT("[ERR] HMC5583: works only single mode");
         return errocode;
     }

     DEBUG_PRINT("[LOG] HMC5583: HMC5883_init OK");
     return 0;
 }
uint8_t HMC5883_get_value(HMC5883_mode_typedef used_mode, HMC5883_data_typedef *values)
{
	HAL_StatusTypeDef errocode = HAL_ERROR;
    uint8_t buf[6] = {0};
    uint8_t cmd[2] = {0};
    
     if (HMC5883_SINGLE_MODE == used_mode)
     {
       	 cmd[0] = HMC5883_MODE_ADDR;
         cmd[1] = HMC5883_DEF_SINGLE_MODE_VAL;

         errocode = HAL_I2C_Master_Transmit(&hi2c1, HMC5883_I2C_ADDR_W, cmd, 2, HMC5883_I2C_TIMEOUT_MS);
         if (HAL_OK != errocode)
         {
             DEBUG_PRINT("[ERR] HMC5583: error write to HMC5883_MODE_ADDR errocode=%d", errocode);
             return errocode;
         }
         
         errocode = HAL_I2C_Mem_Read(&hi2c1,
                                    (uint16_t)HMC5883_I2C_ADDR,
                                    (uint16_t)HMC5883_X_MSB_ADDR,
                                            I2C_MEMADD_SIZE_8BIT,
                                            buf, 6, HMC5883_I2C_TIMEOUT_MS);
         if (errocode != HAL_OK)
         {
             DEBUG_PRINT("[ERR] HMC5583: HAL_I2C_Mem_Read error_code=%d", errocode);
             return errocode;
         }
         else
         {
             values->x_val = 0;
             values->y_val = 0;
             values->z_val = 0;
             
             values->x_val |= (((uint16_t)buf[0]) << 8);
             values->x_val |= buf[1];
             values->y_val |= (((uint16_t)buf[2]) << 8);
             values->y_val |= buf[3];
             values->z_val |= (((uint16_t)buf[4]) << 8);
             values->z_val |= buf[5];
                         
             DEBUG_PRINT("[LOG] HMC5583: x_val=%d y_val=%d z_val=%d", values->x_val, values->y_val, values->z_val);
             return 0;
         }
     }
     else
     {
         DEBUG_PRINT("[ERR] HMC5583: works only single mode");
         return HAL_ERROR;
     }
}
