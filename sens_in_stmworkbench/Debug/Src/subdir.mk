################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Src/BME280.c \
../Src/HMC5883.c \
../Src/ML8511.c \
../Src/bsp_adc.c \
../Src/main.c \
../Src/mq135.c \
../Src/prox_sens.c \
../Src/stm32f4xx_hal_msp.c \
../Src/stm32f4xx_it.c \
../Src/system_stm32f4xx.c 

OBJS += \
./Src/BME280.o \
./Src/HMC5883.o \
./Src/ML8511.o \
./Src/bsp_adc.o \
./Src/main.o \
./Src/mq135.o \
./Src/prox_sens.o \
./Src/stm32f4xx_hal_msp.o \
./Src/stm32f4xx_it.o \
./Src/system_stm32f4xx.o 

C_DEPS += \
./Src/BME280.d \
./Src/HMC5883.d \
./Src/ML8511.d \
./Src/bsp_adc.d \
./Src/main.d \
./Src/mq135.d \
./Src/prox_sens.d \
./Src/stm32f4xx_hal_msp.d \
./Src/stm32f4xx_it.d \
./Src/system_stm32f4xx.d 


# Each subdirectory must supply rules for building sources it contributes
Src/%.o: ../Src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F407xx -I"/home/zaidshakil/Downloads/polaris-zaids-code_stm32-cd41cdb511b50392ff926b21f427bac48d54f46f/sens_in_stmworkbench/Inc" -I"/home/zaidshakil/Downloads/polaris-zaids-code_stm32-cd41cdb511b50392ff926b21f427bac48d54f46f/sens_in_stmworkbench/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/home/zaidshakil/Downloads/polaris-zaids-code_stm32-cd41cdb511b50392ff926b21f427bac48d54f46f/sens_in_stmworkbench/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/home/zaidshakil/Downloads/polaris-zaids-code_stm32-cd41cdb511b50392ff926b21f427bac48d54f46f/sens_in_stmworkbench/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/home/zaidshakil/Downloads/polaris-zaids-code_stm32-cd41cdb511b50392ff926b21f427bac48d54f46f/sens_in_stmworkbench/Drivers/CMSIS/Include"  -Os -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


