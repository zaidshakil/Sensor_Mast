/******************************************************************************
 * \file bsp_adc.c
 * \authors
 *
 * \brief BSP for adc with dma.
 *
 ******************************************************************************/
#include "bsp_adc.h"
#include "debug.h"

 /******************************************************************************
 *                        	 LOCAL DEFINES
 ******************************************************************************/
#define COUNT_TO_MAKE_AVERAGE							(4)
#define ANALOG_INPUTS_COUNT								(4)

#define ST_VREFINT_CAL									((uint32_t)0x1FFF7A2A)
#define ADC_MCU_REF_3300_MV								(3300)
#define ADC_MCU_REF_FULL_SCALE							(0xFFF)
 /******************************************************************************
 *                           LOCAL TYPES
 ******************************************************************************/

 /******************************************************************************
 *                           LOCAL DATAS
 ******************************************************************************/
static uint32_t dma_buffer_for_adc[COUNT_TO_MAKE_AVERAGE*ANALOG_INPUTS_COUNT];
static uint16_t adc_last_values[ANALOG_INPUTS_COUNT];	//vref, in1, in2, in3 ...

extern ADC_HandleTypeDef hadc1;
 /******************************************************************************
 *                        LOCAL FUNCTIONS PROTOTYPES
 ******************************************************************************/
 static void parce_dma_duf (void);
 /******************************************************************************
 *                        LOCAL FUNCTIONS
 ******************************************************************************/
 //parce dma_buffer, make average and store datas in global structs
 static void parce_dma_duf (void)
 {
 	uint8_t i, j;
 	uint32_t sum[ANALOG_INPUTS_COUNT] = {0};

 	for (j = 0; j < ANALOG_INPUTS_COUNT; j++)
 	{
 		for (i = j; i < (COUNT_TO_MAKE_AVERAGE*ANALOG_INPUTS_COUNT); i+=ANALOG_INPUTS_COUNT)
 		{
 			sum[j] += dma_buffer_for_adc[i];
 			//dma_buffer_for_adc[i] = 0;
 		}

 		adc_last_values[j] = (uint16_t)(sum[j] / COUNT_TO_MAKE_AVERAGE);
 	}
 }
 /******************************************************************************
 *                        GLOBAL FUNCTIONS
 ******************************************************************************/
 //make measure and store results in buffer by dma
 uint8_t make_adc_measure(void)
 {
	 HAL_StatusTypeDef error_code = HAL_ERROR;

	 DEBUG_PRINT("[LOG] ADC: make_adc_measure");

 	 error_code = HAL_ADC_Start_DMA(&hadc1, dma_buffer_for_adc, (COUNT_TO_MAKE_AVERAGE)*(ANALOG_INPUTS_COUNT));
	 if (error_code != HAL_OK)
	 {
		 DEBUG_PRINT("[ERR] ADC: HAL_ADC_Start_DMA %d", error_code);
	 }
     
      error_code = HAL_ADC_Start(&hadc1);
	 if (error_code != HAL_OK)
	 {
		 DEBUG_PRINT("[ERR] ADC: HAL_ADC_Start %d", error_code);
	 }

 	return 0;
 }

 void get_data(uint16_t *val_0, uint16_t *val_1, uint16_t *val_2, uint16_t *val_3)
 {
	 uint16_t vref_cal = (*(__IO uint16_t*)(ST_VREFINT_CAL));

	 *val_0 = (((1ULL) * ADC_MCU_REF_3300_MV * vref_cal)/adc_last_values[0]);
	 //*val_0 = 2900;
	 *val_1 = (((1ULL) * *val_0 * adc_last_values[1])/ADC_MCU_REF_FULL_SCALE);
	 *val_2 = (((1ULL) * *val_0 * adc_last_values[2])/ADC_MCU_REF_FULL_SCALE);
	 *val_3 = (((1ULL) * *val_0 * adc_last_values[3])/ADC_MCU_REF_FULL_SCALE);

	#ifdef USE_DEBUG
		DEBUG_PRINT("[LOG] ADC: VREF = %d mv", *val_0);
		DEBUG_PRINT("[LOG] ADC: IN1 = %d mv", *val_1);
		DEBUG_PRINT("[LOG] ADC: IN2 = %d mv", *val_2);
		DEBUG_PRINT("[LOG] ADC: IN3 = %d mv", *val_3);
	#endif
 }

 void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
 {
	 HAL_StatusTypeDef error_code = HAL_ERROR;

	 DEBUG_PRINT("[LOG] ADC: HAL_ADC_ConvCpltCallback");

	 error_code = HAL_ADC_Stop(hadc);
	 if (error_code != HAL_OK)
	 {
		 DEBUG_PRINT("[ERR] ADC: HAL_ADC_Stop %d", error_code);
	 }
	 error_code = HAL_ADC_Stop_DMA(hadc);
	 if (error_code != HAL_OK)
	 {
		 DEBUG_PRINT("[ERR] ADC: HAL_ADC_Stop_DMA %d", error_code);
	 }
 	parce_dma_duf();
 }


 void HAL_ADC_ErrorCallback(ADC_HandleTypeDef* hadc)
 {
 	 DEBUG_PRINT ("[ERR] ADC error \n");
 }
