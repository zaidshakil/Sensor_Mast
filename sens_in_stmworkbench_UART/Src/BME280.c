/******************************************************************************
 * \file BME280.c
 * \authors 
 * 
 * \brief 
 *
 ******************************************************************************/
#include "BME280.h"
#include "debug.h"
 /******************************************************************************
 *                        	 LOCAL DEFINES
 ******************************************************************************/

///defines the connection of SDO pin: SDO->GND: 0x76; SDO->Vcc: 0x77
#define BME280_I2C_ADDR      		(0x76)
///device address
#define BME280_I2C_ADDR_W			0xEC
#define BME280_I2C_ADDR_R			0xED

#define BME280_I2C_TIMEOUT_MS  		(500)
 
 ///register address
#define BME280_ID_ADDR    			(0xD0)
#define BME280_CONFIG_ADDR    		(0xF5)
#define BME280_CTRL_MEAS_ADDR   	(0xF4)
#define BME280_CTRL_HUM_ADDR   		(0xF2)
#define BME280_DATA_BEGIN			(0xF7)	///3 temp, 3 press, 3 hum
#define BME280_DATA_SIZE			(0x08)

#define BME280_TRIM1_BEGIN			(0x88)
#define BME280_TRIM1_SIZE			(26)
#define BME280_TRIM2_BEGIN			(0xE1)
#define BME280_TRIM2_SIZE			(7)
 

  /******************************************************************************
 *                           LOCAL TYPES
 ******************************************************************************/
typedef uint32_t BME280_U32_t;
typedef int32_t BME280_S32_t;
typedef int64_t BME280_S64_t;
typedef uint64_t BME280_U64_t;
 /******************************************************************************
 *                           LOCAL DATAS
 ******************************************************************************/
 extern I2C_HandleTypeDef hi2c1;
 uint16_t dig_T1;
 int16_t dig_T2;
 int16_t dig_T3;
 uint16_t dig_P1;
 int16_t dig_P2;
 int16_t dig_P3;
 int16_t dig_P4;
 int16_t dig_P5;
 int16_t dig_P6;
 int16_t dig_P7;
 int16_t dig_P8;
 int16_t dig_P9;
 uint16_t dig_H1;
 int16_t dig_H2;
 uint16_t dig_H3;
 int16_t dig_H4;
 int16_t dig_H5;
 int16_t dig_H6;

  /******************************************************************************
 *                        LOCAL FUNCTIONS PROTOTYPES
 ******************************************************************************/

 /******************************************************************************
 *                        LOCAL FUNCTIONS 
 ******************************************************************************/

 // Returns temperature in DegC, resolution is 0.01 DegC. Output value of �5123� equals 51.23 DegC.
 // t_fine carries fine temperature as global value
 BME280_S32_t t_fine;
 BME280_S32_t BME280_compensate_T_int32(BME280_S32_t adc_T)
 {
 BME280_S32_t var1, var2, T;
 var1 = ((((adc_T>>3) - ((BME280_S32_t)dig_T1<<1))) * ((BME280_S32_t)dig_T2)) >> 11;
 var2 = (((((adc_T>>4) - ((BME280_S32_t)dig_T1)) * ((adc_T>>4) - ((BME280_S32_t)dig_T1))) >> 12) *
 ((BME280_S32_t)dig_T3)) >> 14;
 t_fine = var1 + var2;
 T = (t_fine * 5 + 128) >> 8;
 return T;
 }
 // Returns pressure in Pa as unsigned 32 bit integer in Q24.8 format (24 integer bits and 8 fractional bits).
 // Output value of �24674867� represents 24674867/256 = 96386.2 Pa = 963.862 hPa
 BME280_U32_t BME280_compensate_P_int64(BME280_S32_t adc_P)
 {
 BME280_S64_t var1, var2, p;
 var1 = ((BME280_S64_t)t_fine) - 128000;
 var2 = var1 * var1 * (BME280_S64_t)dig_P6;
 var2 = var2 + ((var1*(BME280_S64_t)dig_P5)<<17);
 var2 = var2 + (((BME280_S64_t)dig_P4)<<35);
 var1 = ((var1 * var1 * (BME280_S64_t)dig_P3)>>8) + ((var1 * (BME280_S64_t)dig_P2)<<12);
 var1 = (((((BME280_S64_t)1)<<47)+var1))*((BME280_S64_t)dig_P1)>>33;
 if (var1 == 0)
 {
 return 0; // avoid exception caused by division by zero
 }
 p = 1048576-adc_P;
 p = (((p<<31)-var2)*3125)/var1;
 var1 = (((BME280_S64_t)dig_P9) * (p>>13) * (p>>13)) >> 25;
 var2 = (((BME280_S64_t)dig_P8) * p) >> 19;
 p = ((p + var1 + var2) >> 8) + (((BME280_S64_t)dig_P7)<<4);
 return (BME280_U32_t)p;
 }
 // Returns humidity in %RH as unsigned 32 bit integer in Q22.10 format (22 integer and 10 fractional bits).
 // Output value of �47445� represents 47445/1024 = 46.333 %RH
 BME280_U32_t bme280_compensate_H_int32(BME280_S32_t adc_H)
 {
 BME280_S32_t v_x1_u32r;
 v_x1_u32r = (t_fine - ((BME280_S32_t)76800));
 v_x1_u32r = (((((adc_H << 14) - (((BME280_S32_t)dig_H4) << 20) - (((BME280_S32_t)dig_H5) * v_x1_u32r)) +
 ((BME280_S32_t)16384)) >> 15) * (((((((v_x1_u32r * ((BME280_S32_t)dig_H6)) >> 10) * (((v_x1_u32r *
 ((BME280_S32_t)dig_H3)) >> 11) + ((BME280_S32_t)32768))) >> 10) + ((BME280_S32_t)2097152)) *
 ((BME280_S32_t)dig_H2) + 8192) >> 14));
 v_x1_u32r = (v_x1_u32r - (((((v_x1_u32r >> 15) * (v_x1_u32r >> 15)) >> 7) * ((BME280_S32_t)dig_H1)) >> 4));
 v_x1_u32r = (v_x1_u32r < 0 ? 0 : v_x1_u32r);
 v_x1_u32r = (v_x1_u32r > 419430400 ? 419430400 : v_x1_u32r);
 return (BME280_U32_t)(v_x1_u32r>>12);
 }

 void BME280_Read_calibatiron_values()
 {
	 HAL_StatusTypeDef errocode = HAL_ERROR;
	 	uint8_t data_array[BME280_TRIM1_SIZE] = {0};
	 	uint8_t data_array_[BME280_TRIM2_SIZE] = {0};

	     errocode = HAL_I2C_Mem_Read(&hi2c1,
	                                (uint16_t)BME280_I2C_ADDR_R,
	                                (uint16_t)BME280_TRIM1_BEGIN,
	                                        I2C_MEMADD_SIZE_8BIT,
	 									   data_array, BME280_TRIM1_SIZE, BME280_I2C_TIMEOUT_MS);

	     if (errocode != HAL_OK)
	     {
	         DEBUG_PRINT("[ERR] BME280: BME280_Read_calibatiron_values error_code=%d", errocode);
	     }
	     else
	     {
	     	//uncompensated
	    	 dig_T1 = (uint16_t)((data_array[0] | ((uint16_t)data_array[1] << 8)));
	    	 dig_T2 = (int16_t)(data_array[2] | ((uint16_t)data_array[3] << 8));
	    	 dig_T3 = (int16_t)(data_array[4] | ((uint16_t)data_array[5] << 8));

	    	 dig_P1 = (uint16_t)(data_array[6] | ((uint16_t)data_array[7] << 8));
	    	 dig_P2 = (int16_t)(data_array[8] | ((uint16_t)data_array[9] << 8));
	    	 dig_P3 = (int16_t)(data_array[10] | ((uint16_t)data_array[11] << 8));
	    	 dig_P4 = (int16_t)(data_array[12] | ((uint16_t)data_array[13] << 8));
	    	 dig_P5 = (int16_t)(data_array[14] | ((uint16_t)data_array[15] << 8));
	    	 dig_P6 = (int16_t)(data_array[16] | ((uint16_t)data_array[17] << 8));
	    	 dig_P7 = (int16_t)(data_array[18] | ((uint16_t)data_array[19] << 8));
	    	 dig_P8 = (int16_t)(data_array[20] | ((uint16_t)data_array[21] << 8));
	    	 dig_P9 = (int16_t)(data_array[22] | ((uint16_t)data_array[23] << 8));

	    	 dig_H1 = (uint16_t)data_array[24];

	     }


	     errocode = HAL_I2C_Mem_Read(&hi2c1,
	    	                                (uint16_t)BME280_I2C_ADDR_R,
	    	                                (uint16_t)BME280_TRIM2_BEGIN,
	    	                                        I2C_MEMADD_SIZE_8BIT,
	    	 									   data_array_, BME280_TRIM2_SIZE, BME280_I2C_TIMEOUT_MS);

	    	     if (errocode != HAL_OK)
	    	     {
	    	         DEBUG_PRINT("[ERR] BME280: BME280_Read_calibatiron_values error_code=%d", errocode);
	    	     }
	    	     else
	    	     {
	    	     	//uncompensated
	    	    	 dig_H2 = (int16_t)(data_array_[0] | ((uint16_t)data_array_[1] << 8));
	    	    	 dig_H3 = (uint16_t)(data_array_[2]);
	    	    	 dig_H4 = (int16_t)(( data_array_[3] << 4 ) | ( (uint8_t)0x0F & (uint16_t)data_array_[4] << 8));
	    	    	 dig_H5 = (int16_t)(((int8_t)data_array_[5] << 4) | ((uint16_t)data_array_[6] >> 4));
	    	    	 dig_P6 = (int16_t)data_array_[6];
	    	     }
                 
                 
	  DEBUG_PRINT("[LOG] BME280: BME280_Read_calibatiron_values OK");
 }
 /******************************************************************************
 *                        GLOBAL FUNCTIONS 
 ******************************************************************************/
 uint8_t BME280_init()
 {
	 HAL_StatusTypeDef errocode = HAL_ERROR;
	 uint8_t cmd[2] = {0};

     errocode = HAL_I2C_IsDeviceReady(&hi2c1, BME280_I2C_ADDR_R, 5, BME280_I2C_TIMEOUT_MS);
     if (HAL_OK != errocode)
     {
    	 DEBUG_PRINT("[ERR] BME280: error HAL_I2C_IsDeviceReady errocode=%d", errocode);
    	 //return errocode;
     }

	 cmd[0] = BME280_CTRL_HUM_ADDR;
     cmd[1] = 0x01;	//x1 oversampling

	 errocode = HAL_I2C_Master_Transmit(&hi2c1, BME280_I2C_ADDR_W, cmd, 2, BME280_I2C_TIMEOUT_MS);
  	if (HAL_OK != errocode)
    {
        DEBUG_PRINT("[ERR] BME280: error write to BME280_CTRL_HUM_ADDR errocode=%d", errocode);
        return errocode;
    }
    
   	errocode = HAL_I2C_Mem_Read(&hi2c1, (uint16_t)BME280_I2C_ADDR_R, BME280_CTRL_MEAS_ADDR, I2C_MEMADD_SIZE_8BIT, &cmd[0], 1, BME280_I2C_TIMEOUT_MS);
 	if (HAL_OK != errocode)
   {
       DEBUG_PRINT("[ERR] BME280: error write to BME280_CTRL_MEAS_ADDR errocode=%d", errocode);
       return errocode;
   }

	cmd[0] = BME280_CTRL_MEAS_ADDR;
    cmd[1] = 0x7F;	//pres x16 oversampling, temp x4 oversampling, normal mode

	errocode = HAL_I2C_Master_Transmit(&hi2c1, BME280_I2C_ADDR_W, cmd, 2, BME280_I2C_TIMEOUT_MS);
 	if (HAL_OK != errocode)
   {
       DEBUG_PRINT("[ERR] BME280: error write to BME280_CTRL_MEAS_ADDR errocode=%d", errocode);
       return errocode;
   }
   
   	errocode = HAL_I2C_Mem_Read(&hi2c1, (uint16_t)BME280_I2C_ADDR_R, BME280_CTRL_MEAS_ADDR, I2C_MEMADD_SIZE_8BIT, &cmd[0], 1, BME280_I2C_TIMEOUT_MS);
 	if (HAL_OK != errocode)
   {
       DEBUG_PRINT("[ERR] BME280: error write to BME280_CTRL_MEAS_ADDR errocode=%d", errocode);
       return errocode;
   }
   
   

	cmd[0] = BME280_CONFIG_ADDR;
    cmd[1] = 0x08;	//t stby = 0.5ms, filter = 16

	errocode = HAL_I2C_Master_Transmit(&hi2c1, BME280_I2C_ADDR_W, cmd, 2, BME280_I2C_TIMEOUT_MS);
 	if (HAL_OK != errocode)
   {
       DEBUG_PRINT("[ERR] BME280: error write to BME280_CONFIG_ADDR errocode=%d", errocode);
       return errocode;
   }
   
    BME280_Read_calibatiron_values();
     
    DEBUG_PRINT("[LOG] BME280: init done");
     return 0;
 }


uint8_t BME280_get_value(int32_t *temp, uint32_t *press, uint32_t *hum)
{
	HAL_StatusTypeDef errocode = HAL_ERROR;
	uint8_t data_array[BME280_DATA_SIZE] = {0};
	int32_t t, p, h;
   
    errocode = HAL_I2C_Mem_Read(&hi2c1,
                               (uint16_t)BME280_I2C_ADDR_R,
                               (uint16_t)BME280_DATA_BEGIN,
                                       I2C_MEMADD_SIZE_8BIT,
									   data_array, BME280_DATA_SIZE, BME280_I2C_TIMEOUT_MS);

    if (errocode != HAL_OK)
    {
        DEBUG_PRINT("[ERR] BME280: BME280_DATA_BEGIN error_code=%d", errocode);
        return errocode;
    }
    else
    {
    	//uncompensated
    	p = (int32_t)(  ((uint32_t)data_array[2] >> 4) |
    						((uint32_t)data_array[1] << 4) |
							((uint32_t)data_array[0] << 12));

    	t = (int32_t)( ((uint32_t)data_array[5] >> 4) |
    	    				((uint32_t)data_array[4] << 4) |
    						((uint32_t)data_array[3] << 12));

    	h   = (int32_t)( ((uint32_t)data_array[7] |
    	    				((uint32_t)data_array[6] << 8)));

    	*temp = BME280_compensate_T_int32(t);
    	*press = BME280_compensate_P_int64(p);
    	*hum = bme280_compensate_H_int32(h);

    	DEBUG_PRINT("[LOG] BME280: UNCOMP T=%d, P=%d, H=%d", *temp, *press, *hum);

        return 0;
    }

}
