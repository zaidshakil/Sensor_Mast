/******************************************************************************
 * \file HMC5883.h
 * \authors 
 * 
 * \brief driver hmc5883
 *
 ******************************************************************************/
#ifndef __MQ135__
#define __MQ135__

/******************************************************************************
 *                             HEADER FILES
 ******************************************************************************/

 /******************************************************************************
 *                        	 GLOBAL DEFINES
 ******************************************************************************/
 
 /******************************************************************************
 *                           GLOBAL TYPES
 ******************************************************************************/

 /******************************************************************************
 *                        GLOBAL FUNCTIONS PROTOTYPES
 ******************************************************************************/

 double get_co2_level(uint16_t voltage_lvl);

#endif
